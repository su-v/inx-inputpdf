#!/usr/bin/env python
"""
input_pdf_ext - Python script for custom PDF import in Inkscape
                via pdftocairo from poppler

Copyright (C) 2014-2016, su_v <suv-sf@users.sf.net>


Originally based on:
 ps2pdf-ext.py  - Python script for running ps2pdf in
                  Inkscape extensions
 run_command.py - Module for running SVG-generating commands in
                  Inkscape extensions

 Copyright (C) 2008 Stephen Silver


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring

# standard library
import sys
import os

# local library
import inkex
from svg_fallbacks import SVGpageSet2layers
from svg_fallbacks import check_svg_version


PAPERSIZES = {'match': (595, 842),
              'A4': (595, 842),
              'A3': (842, 1191),
              'letter': (612, 792),
              'legal': (612, 1008)}


def run_with_stdin(command_format, stream, prog_name, verbose=False):
    """Run command using stdin instead of file as input."""
    # pylint: disable=broad-except
    if verbose:
        inkex.debug(command_format)
    msg = None
    try:
        try:
            from subprocess import Popen, PIPE
            if isinstance(command_format, list):
                p = Popen(command_format, shell=False,
                          stdin=PIPE, stdout=PIPE, stderr=PIPE)
                out, err = p.communicate(input=stream)
            elif isinstance(command_format, str):
                p = Popen(command_format, shell=True,
                          stdin=PIPE, stdout=PIPE, stderr=PIPE)
                out, err = p.communicate(input=stream)
            else:
                msg = "unsupported command format %s" % type(command_format)
        except ImportError:
            msg = "subprocess.Popen not available"
        if err and msg is None:
            msg = "%s failed:\n%s\n%s\n" % (prog_name, out, err)
    except Exception as inst:
        msg = "Error attempting to run %s: %s" % (prog_name, str(inst))
    if msg is None:
        return out
    else:
        inkex.errormsg(msg)
        sys.exit(1)


def run(command_format, prog_name, verbose=False):
    """Run command."""
    # pylint: disable=broad-except
    if verbose:
        inkex.debug(command_format)
    msg = None
    try:
        try:
            from subprocess import Popen, PIPE
            if isinstance(command_format, list):
                p = Popen(command_format, shell=False,
                          stdout=PIPE, stderr=PIPE)
                out, err = p.communicate()
            elif isinstance(command_format, str):
                p = Popen(command_format, shell=True,
                          stdout=PIPE, stderr=PIPE)
                out, err = p.communicate()
            else:
                msg = "unsupported command format %s" % type(command_format)
        except ImportError:
            msg = "subprocess.Popen not available"
        if err and msg is None:
            msg = "%s failed:\n%s\n%s\n" % (prog_name, out, err)
    except Exception as inst:
        msg = "Error attempting to run %s: %s" % (prog_name, str(inst))
    if msg is None:
        return out
    else:
        inkex.errormsg(msg)
        sys.exit(1)


def parse_svgstring(svgstring):
    """Return ElementTree object obtained by parsing *svgstring*."""
    p = inkex.etree.XMLParser(huge_tree=True)
    return inkex.etree.ElementTree(inkex.etree.fromstring(svgstring, parser=p))


class Document(object):
    # pylint: disable=too-few-public-methods
    pass


class SVGDocument(Document):
    # pylint: disable=too-few-public-methods
    pass


class PDFDocument(Document):
    """Class for PDF files to be imported into Inkscape."""

    def __init__(self, pdf_file):
        """Init with file path to PDF file."""
        self.pdf_file = pdf_file

    def _pdftocairo_import(self, opts, verbose=False):
        """Return output of pdftocairo as string."""
        cmd = list(opts)
        cmd.append('%s' % self.pdf_file)
        cmd.append('-')
        return run(cmd, cmd[0], verbose)

    def _pdf_import_with(self, opts, verbose=False):
        """Dispatch pdf import command."""
        import_method = getattr(
            self, '_{}_import'.format(opts[0]), self._pdftocairo_import)
        return import_method(opts, verbose)

    def to_svg(self, opts, verbose=False):
        """Return ElementTree object from external command."""
        return parse_svgstring(self._pdf_import_with(opts, verbose))

    def info(self, verbose=False):
        """Return output of external command 'pdfinfo'."""
        cmd = ['pdfinfo', ]
        cmd.append('-box')
        cmd.append('%s' % self.pdf_file)
        return run(cmd, cmd[0], verbose)


class PDFStream(PDFDocument):
    """Class for PDF documents to be imported into Inkscape from stdin."""

    def __init__(self, pdf_stream):
        """Init with stream of PDF content."""
        PDFDocument.__init__(self, None)
        self.pdf_stream = pdf_stream

    def _pdftocairo_import(self, opts, verbose=False):
        """Return output of pdftocairo as string."""
        cmd = list(opts)
        cmd.append('-')  # stdin
        cmd.append('-')  # stdout
        return run_with_stdin(cmd, self.pdf_stream, cmd[0], verbose)

    def info(self, verbose=False):
        """Return output of external command 'pdfinfo'."""
        pass


class PDF2SVG(inkex.Effect):
    """Class to read SVG output of pdftocairo from stdout.

    Optionally post-process <pageSet> (SVG 1.2) to Inkscape layers.
    """
    def __init__(self):
        # Call base class construtor.
        inkex.Effect.__init__(self)
        self.verbose = None

        # pdftocairo options
        # page range
        self.OptionParser.add_option("--page_number",
                                     action="store", type="int",
                                     dest="page_number", default=1,
                                     help="")
        self.OptionParser.add_option("--first_page",
                                     action="store", type="int",
                                     dest="first_page", default=1,
                                     help="")
        self.OptionParser.add_option("--last_page",
                                     action="store", type="int",
                                     dest="last_page", default=10,
                                     help="")
        self.OptionParser.add_option("--pages_even_odd",
                                     action="store", type="string",
                                     dest="pages_even_odd", default="odd",
                                     help="")
        # paper size
        self.OptionParser.add_option("--paper",
                                     action="store", type="string",
                                     dest="paper", default="match",
                                     help="")
        self.OptionParser.add_option("--paper_width",
                                     action="store", type="int",
                                     dest="paper_width", default=595,
                                     help="")
        self.OptionParser.add_option("--paper_height",
                                     action="store", type="int",
                                     dest="paper_height", default=842,
                                     help="")
        self.OptionParser.add_option("--use_fonthack",
                                     action="store", type="inkbool",
                                     dest="use_fonthack", default=False,
                                     help="")
        self.OptionParser.add_option("--fonthack_factor",
                                     action="store", type="int",
                                     dest="fonthack_factor", default=10,
                                     help="")
        # crop area
        self.OptionParser.add_option("--crop_x",
                                     action="store", type="int",
                                     dest="crop_x", default=0,
                                     help="")
        self.OptionParser.add_option("--crop_y",
                                     action="store", type="int",
                                     dest="crop_y", default=0,
                                     help="")
        self.OptionParser.add_option("--crop_width",
                                     action="store", type="int",
                                     dest="crop_width", default=595,
                                     help="")
        self.OptionParser.add_option("--crop_height",
                                     action="store", type="int",
                                     dest="crop_height", default=842,
                                     help="")
        self.OptionParser.add_option("--nocrop",
                                     action="store", type="inkbool",
                                     dest="nocrop", default=False,
                                     help="")
        # layout (page scaling)
        self.OptionParser.add_option("--expand",
                                     action="store", type="inkbool",
                                     dest="expand", default=False,
                                     help="")
        self.OptionParser.add_option("--shrink",
                                     action="store", type="inkbool",
                                     dest="shrink", default=True,
                                     help="")
        self.OptionParser.add_option("--center",
                                     action="store", type="inkbool",
                                     dest="center", default=True,
                                     help="")
        # pdfinfo
        self.OptionParser.add_option("--use_pdfinfo",
                                     action="store", type="inkbool",
                                     dest="use_pdfinfo", default=False,
                                     help="")
        # bitmaps
        self.OptionParser.add_option("--resolution",
                                     action="store", type="float",
                                     dest="resolution", default=150.0,
                                     help="")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")
        self.OptionParser.add_option("--notebook_pages",
                                     action="store", type="string",
                                     dest="notebook_pages")
        self.OptionParser.add_option("--notebook_paper",
                                     action="store", type="string",
                                     dest="notebook_paper")
        self.OptionParser.add_option("--notebook_crop_area",
                                     action="store", type="string",
                                     dest="notebook_crop_area")
        # SVG output
        self.OptionParser.add_option("--use_layers",
                                     action="store", type="inkbool",
                                     dest="use_layers", default=True,
                                     help="")
        # override custom options
        self.OptionParser.add_option("--use_defaults",
                                     action="store", type="inkbool",
                                     dest="use_defaults", default=True,
                                     help="")
        # debug
        self.OptionParser.add_option("--verbose",
                                     action="store", type="inkbool",
                                     dest="verbose", default=False,
                                     help="")

    # methods

    def get_pdftocairo_opts(self):
        """Build args list for pdftocairo, based on self.options."""

        def _input_pdf_opts_pages(opts):
            """appends opts for page range"""
            if self.options.notebook_pages == '"tab_pages_number"':
                opts.append('-f')
                opts.append(str(self.options.page_number))
                opts.append('-l')
                opts.append(str(self.options.page_number))
            elif self.options.notebook_pages == '"tab_pages_range"':
                opts.append('-f')
                opts.append(str(self.options.first_page))
                opts.append('-l')
                opts.append(str(self.options.last_page))
            elif self.options.notebook_pages == '"tab_pages_even_odd"':
                if self.options.pages_even_odd == "even":
                    opts.append('-e')
                else:
                    opts.append('-o')

        def _input_pdf_opts_paper(opts):
            """Append opts for paper size."""
            if not self.options.use_fonthack:
                if self.options.notebook_paper == '"tab_paper_custom"':
                    opts.append('-paperw')
                    opts.append(str(self.options.paper_width))
                    opts.append('-paperh')
                    opts.append(str(self.options.paper_height))
                else:  # presets
                    opts.append('-paper')
                    opts.append(self.options.paper)
            else:  # use fonthack
                if self.options.notebook_paper == '"tab_paper_custom"':
                    width = (self.options.paper_width *
                             self.options.fonthack_factor)
                    height = (self.options.paper_height *
                              self.options.fonthack_factor)
                else:  # presets
                    try:
                        width = (PAPERSIZES[self.options.paper][0] *
                                 self.options.fonthack_factor)
                        height = (PAPERSIZES[self.options.paper][1] *
                                  self.options.fonthack_factor)
                    except IndexError:
                        # paper not in PAPERSIZES: fall back to A4
                        width = (PAPERSIZES['A4'][0] *
                                 self.options.fonthack_factor)
                        height = (PAPERSIZES['A4'][1] *
                                  self.options.fonthack_factor)
                opts.append('-paperw')
                opts.append(str(width))
                opts.append('-paperh')
                opts.append(str(height))

        def _input_pdf_opts_cropbox(opts):
            """Append opts for cropBox."""
            if self.options.notebook_crop_area == '"tab_crop_area_custom"':
                opts.append('-x')
                opts.append(str(self.options.crop_x))
                opts.append('-y')
                opts.append(str(self.options.crop_y))
                opts.append('-W')
                opts.append(str(self.options.crop_width))
                opts.append('-H')
                opts.append(str(self.options.crop_height))
            if self.options.nocrop:
                opts.append('-nocrop')

        def _input_pdf_opts_scaling(opts):
            """Append opts for scaling."""
            if self.options.expand or self.options.use_fonthack:
                opts.append('-expand')
            if not self.options.shrink:
                opts.append('-noshrink')
            if not self.options.center:
                opts.append('-nocenter')

        def _input_pdf_opts_bitmap(opts):
            """Append opts for bitmaps."""
            if self.options.resolution > 0:
                opts.append('-r')
                opts.append(str(self.options.resolution))

        opts = ['pdftocairo', '-svg']
        if not self.options.use_defaults:
            _input_pdf_opts_pages(opts)
            _input_pdf_opts_paper(opts)
            _input_pdf_opts_cropbox(opts)
            _input_pdf_opts_scaling(opts)
            _input_pdf_opts_bitmap(opts)
        return opts

    def cairo_svg_to_inkscape_svg(self):
        """Run tasks for post-processing pdftocairo's SVG output."""

        # helper

        def _create_namedview():
            """Create <sodipodi:namedview> element in document root."""
            root = self.document.getroot()
            namedview = root.find(inkex.addNS('namedview', 'sodipodi'))
            if namedview is None:
                namedview = inkex.etree.Element(
                    inkex.addNS('namedview', 'sodipodi'))
                root.insert(0, namedview)
            return namedview

        def _group_to_layer(group, layer_id, layer_label):
            """Convert group to Inkscape layer, with provided id and label."""
            group.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
            group.set(inkex.addNS('label', 'inkscape'), layer_label)
            group.set('id', self.uniqueId(layer_id))

        def _groups_with_id_to_layers(node):
            """Search in node for <g> elements, convert groups to layer."""
            groups = node.xpath('//svg:g', namespaces=inkex.NSS)
            for group in groups:
                if 'id' in group.attrib:
                    _group_to_layer(group, group.get('id'), group.get('id'))

        def _cairo_groups_to_layers(root):
            """Post-process SVG output of pdftocairo (groups to layers)."""
            _groups_with_id_to_layers(root)

        def _set_current_layer(layer):
            """Set namedview attribute for current layer."""
            if 'id' in layer.attrib:
                self.getNamedView().set(
                    inkex.addNS('current-layer', 'inkscape'), layer.get('id'))

        def _cairo_pages_to_layers(root):
            """Post-process SVG output of pdftocairo (pages to layers)."""
            pageset = SVGpageSet2layers(root)
            last_layer = pageset.pages_to_layers()
            if 'id' not in last_layer.attrib:
                last_layer.set('id', self.uniqueId("pageSet"))
            _set_current_layer(last_layer)

        # functions

        def default_namedview():
            """Set document properties (common defaults)."""
            namedview = _create_namedview()
            namedview.set('id', 'base')
            namedview.set('bordercolor', '#666666')
            namedview.set('borderlayer', 'false')
            namedview.set('borderopacity', '1.0')
            namedview.set(inkex.addNS('pageopacity', 'inkscape'), '0.0')
            namedview.set(inkex.addNS('pageshadow', 'inkscape'), '2')
            namedview.set(inkex.addNS('showpageshadow', 'inkscape'), 'true')
            namedview.set(inkex.addNS('zoom', 'inkscape'), '0.35')
            namedview.set('pagecolor', '#ffffff')
            namedview.set('showborder', 'true')
            namedview.set('showgrid', 'false')
            namedview.set('showguides', 'false')

        def cairo_svg_to_layers():
            """Post-process SVG output of pdftocairo."""
            root = self.document.getroot()
            if root.xpath('//svg:pageSet', namespaces=inkex.NSS):
                _cairo_pages_to_layers(root)
            else:
                _cairo_groups_to_layers(root)

        def adjust_namedview():
            """Post-process namedview attribtues based on import."""
            # pdftocairo output is based on 'pt'
            self.getNamedView().set(
                inkex.addNS('document-units', 'inkscape'), 'pt')
            self.getNamedView().set('units', 'pt')

        def adjust_root():
            """Post-process SVG root attributes based on import."""
            root = self.document.getroot()
            # SVG version
            if self.options.use_layers:
                root.set('version', check_svg_version(root))
            # Font hack (import upscaled by factor,
            #            downscale via SVG width, height, viewBox)
            if self.options.use_fonthack:
                viewbox = list(float(i) for i in root.get('viewBox').split())
                root.set('width', "%spt" %
                         (viewbox[2] / self.options.fonthack_factor))
                root.set('height', "%spt" %
                         (viewbox[3] / self.options.fonthack_factor))

        default_namedview()
        if self.options.use_layers:
            cairo_svg_to_layers()
        adjust_namedview()
        adjust_root()

    # main

    def parse_pdf(self):
        """Run pdftocairo and read SVG output into XML tree."""
        # pylint: disable=redefined-variable-type
        # pylint: disable=protected-access
        self.verbose = self.options.verbose
        if os.path.isfile(sys.argv[-1]):
            pdf_doc = PDFDocument(sys.argv[-1])
        else:
            pdf_doc = PDFStream(sys.stdin)
        if self.options.use_pdfinfo:
            inkex.debug(pdf_doc.info())
        self.document = pdf_doc.to_svg(self.get_pdftocairo_opts(),
                                       self.verbose)
        # exit now if parsing failed (else e.affect() fails anyway):
        if not isinstance(self.document, inkex.etree._ElementTree):
            inkex.errormsg(
                "Ooops - something went wrong: no ElementTree object.")
            sys.exit(1)

    def effect_svg(self):
        """Post-process SVG source imported from pdftocairo output"""
        self.cairo_svg_to_inkscape_svg()

    def output_svg(self):
        """Send post-processed SVG source back to inkscape."""
        if sys.version_info < (3,):
            self.document.write(sys.stdout)
        else:
            self.document.write(sys.stdout.buffer)  # pylint: disable=no-member

    # override inkex.Effect:

    def parse(self, filename=None):
        """Override self.parse from Effect.affect()."""
        self.parse_pdf()

    def effect(self):
        """Override self.effect from Effect.affect()."""
        self.effect_svg()

    def output(self):
        """Override self.output from Effect.affect()."""
        self.output_svg()


if __name__ == '__main__':
    ME = PDF2SVG()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
