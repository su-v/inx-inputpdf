#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
pageset_to_layer - Inkscape extension to convert SVG 1.2 <pageSet>
                   to Inkscape layers

Copyright (C) 2014-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring

# local library
import inkex
from svg_fallbacks import SVGpageSet2layers
from svg_fallbacks import check_svg_version


class Pages2Layers(inkex.Effect):
    def __init__(self):
        # Call base class construtor.
        inkex.Effect.__init__(self)

        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")
        # global
        self.OptionParser.add_option("--verbose",
                                     action="store", type="inkbool",
                                     dest="verbose", default=False,
                                     help="")

    def set_current_layer(self, layer):
        """Set namedview attribute for current layer."""
        if 'id' in layer.attrib:
            self.getNamedView().set(
                inkex.addNS('current-layer', 'inkscape'), layer.get('id'))

    def effect(self):
        root = self.document.getroot()
        if root is not None:
            pageset = SVGpageSet2layers(root)
            last_layer = pageset.pages_to_layers()
            if 'id' not in last_layer.attrib:
                last_layer.set('id', self.uniqueId("pageSet"))
            self.set_current_layer(last_layer)
            root.set('version', check_svg_version(root))


if __name__ == '__main__':
    ME = Pages2Layers()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
