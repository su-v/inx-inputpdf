#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
svg_fallbacks - Inkscape module with classes and helper routines
                for the conversion of SVG 1.2 elements to SVG 1.1
                or Inkscape-specific elements.

Copyright (C) 2014-2016, su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
# pylint: disable=missing-docstring

# local library
from inkex import NSS, addNS, etree, errormsg


def is_pageset(node):
    return node.tag == addNS('pageSet', 'svg')


def is_page(node):
    return node.tag == addNS('page', 'svg')


def is_group(node):
    return node.tag == addNS('g', 'svg')


def is_layer(node):
    return (is_group(node) and
            node.get(addNS('groupmode', 'inkscape')) == 'layer')


def hide_element(ele):
    if ele is not None:
        ele.set('style', "display:none")
    return ele


def show_element(ele):
    if ele is not None:
        ele.set('style', "display:inline")
    return ele


def add_layer(parent, label):
    layergroup = etree.SubElement(parent, addNS('g', 'svg'))
    layergroup.set(addNS('label', 'inkscape'), label)
    layergroup.set(addNS('groupmode', 'inkscape'), 'layer')
    return layergroup


def check_svg_version(root):
    if not root.xpath('//svg:pageSet', namespaces=NSS):
        return "1.1"
    else:
        return "1.2"


class SVGpageSet2layers(object):
    """Class to convert <pageSet> to Inkscape layer structure."""

    def __init__(self, root):
        self.root = root

    def __call__(self):
        return self.pages_to_layers()

    def page_to_layer(self, parent_layer, page, j):
        # pylint: disable=no-self-use
        page_layer = add_layer(parent_layer, "Page %s" % j)
        hide_element(page_layer)
        for item in page.iterchildren():
            page_layer.append(item)
        page.getparent().remove(page)
        return page_layer

    def pageset_to_layer(self, pageset):
        pageset_layer = add_layer(self.root, "PageSet")
        hide_element(pageset_layer)
        last_page_layer = None
        for page in pageset.iterchildren(reversed=True):
            last_page_layer = self.page_to_layer(
                pageset_layer, page, pageset.index(page) + 1)
        pageset.getparent().remove(pageset)
        show_element(last_page_layer)
        return pageset_layer

    def pages_to_layers(self):
        pagesets = []
        pagesets = self.root.xpath('//svg:pageSet', namespaces=NSS)
        if pagesets:
            last_pageset_layer = None
            for index, pageset in enumerate(pagesets):
                if index == 0:
                    last_pageset_layer = self.pageset_to_layer(pageset)
                else:
                    errormsg("Unsupported SVG 1.2 element detected" +
                             "(multiple <pageSet> elements).")
            # self.root.set('version', check_svg_version(self.root))
            return show_element(last_pageset_layer)


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
